<div class="product-grid swatch gilman" itemscope itemtype="http://schema.org/ItemList">
    <div class="row product-row">
        <?php 
        $col_class = 'col-md-4 col-sm-4';
        $K = 1;
       // while ( have_posts() ): the_post(); 
        while ( $prod_list->have_posts() ): $prod_list->the_post(); 
        
        ?>
            <div class="<?php echo $col_class; ?>">    
                <div class="fl-post-grid-post" itemprop="itemListElement" itemscope  itemtype="http://schema.org/ListItem">

            <meta itemprop="position" content="<?php echo $K;?>" />
                <?php if(get_the_post_thumbnail()) { ?>
                    <div class="fl-post-grid-image">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php 
                            $image = get_the_post_thumbnail(get_the_ID(),array(280,360)); 
                            echo $image;
                            ?>
                            
                        </a>
                    </div>
                <?php } else { ?>
                    <div class="fl-post-grid-image">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php //the_post_thumbnail($settings->image_size); ?>
                            <img src="https://gilmanfloors-stg.mm-dev.agency/wp-content/uploads/woocommerce-placeholder-150x150.png" alt="<?php the_title_attribute(); ?>" />
                        </a>
                    </div>
                <?php } ?>
                <div class="fl-post-grid-text product-grid btn-grey">
                    <?php 
                        $terms = get_the_terms( get_the_ID(), 'product_cat' );
                        $product_cat_name ="";
                        if(count($terms) > 0){
                            foreach ($terms  as $term  ) {
                                $product_cat_name .= $term->name.", ";
                            }
                        }    
                    ?>
                    <h4 class="woo_product_category"><?php echo rtrim($product_cat_name,", ");?></h4>
                    <a class="woo_product_title" href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                    <?php 
                       // $species = get_the_terms( get_the_ID(), 'pa_species');
                    ?>
                    <h3 class="woo_product_species"><?php //echo $species[0]->name;?></h3>
                   
                </div>
            </div>
        </div>
    <?php  $K++; endwhile; ?>
    </div>
</div>