<?php

// Defines
define('FL_CHILD_THEME_DIR', get_stylesheet_directory());
define('FL_CHILD_THEME_URL', get_stylesheet_directory_uri());

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action('wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000);

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script("slick", get_stylesheet_directory_uri()."/resources/slick/slick.min.js", "", "", 1);
    wp_enqueue_script("cookie", get_stylesheet_directory_uri()."/resources/jquery.cookie.js", "", "", 1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

//Facet Title Hook
add_filter('facetwp_shortcode_html', function ($output, $atts) {
    if (isset($atts['facet'])) {
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2);



// Register menus
function register_my_menus()
{
    register_nav_menus(
        array(
            'footer-1' => __('Footer Menu 1'),
            'footer-2' => __('Footer Menu 2'),
            'footer-3' => __('Footer Menu 3'),
            'footer-4' => __('Footer Menu 4'),
            'footer-5' => __('Footer Menu 5'),
            'site-map' => __('Site Map'),
        )
    );
}
add_action('init', 'register_my_menus');


 
// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');


function register_shortcodes()
{
    $dir=dirname(__FILE__)."/shortcodes";
    $files=scandir($dir);
    foreach ($files as $k=>$v) {
        $file=$dir."/".$v;
        if (strstr($file, ".php")) {
            $shortcode=substr($v, 0, -4);
            add_shortcode($shortcode, function ($attr, $content, $tag) {
                ob_start();
                include(dirname(__FILE__)."/shortcodes/".$tag.".php");
                $c=ob_get_clean();
                return $c;
            });
        }
    }
}



// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


add_filter('wp_nav_menu_items', 'do_shortcode');


  /**
 * Dequeue the jQuery UI styles.
 *
 * Hooked to the wp_print_styles action, with a late priority (100),
 * so that it is after the style was enqueued.
 */
function remove_pagelist_css() {
    wp_dequeue_style( 'page-list-style' );
 }
 add_action( 'wp_print_styles', 'remove_pagelist_css', 100 );

 //add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}

add_filter( 'auto_update_plugin', '__return_false' );


//Yoast SEO Breadcrumb link - Changes for PDP pages
// add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',90 );

// function wpse_override_yoast_breadcrumb_trail( $links ) {
//     if (is_singular( 'luxury_vinyl_tile' )) {

//         $breadcrumb[] = array(
//             'url' => get_site_url().'/flooring/',
//             'text' => 'Flooring',
//         );
//         $breadcrumb[] = array(
//             'url' => get_site_url().'/flooring/sheet-vinyl/',
//             'text' => 'Luxury Vinyl',
//         );
//         $breadcrumb[] = array(
//             'url' => get_site_url().'/flooring/sheet-vinyl/products/',
//             'text' => 'Products',
//         );
//         array_splice( $links, 1, -1, $breadcrumb );
        
//     }
    
//     return $links;
// }
// 
// 
// 


function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

function cart_price( $atts ){
     global $woocommerce; ?>
    <a class="" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('Cart View', 'woothemes'); ?>">       
        <?php echo $woocommerce->cart->get_cart_total(); ?>
    </a>

    <?php
}
add_shortcode( 'CART-PRICE', 'cart_price' );


add_action( 'woocommerce_before_shop_loop_item_title', 'add_categoryname_product_loop', 25);
function add_categoryname_product_loop() {
    global $product;
    $product_cats = wp_get_post_terms($product->id, 'product_cat');
    $count = count($product_cats);
    foreach($product_cats as $key => $cat)
    {
        echo 
        '
        <span class="cat-name">'.$cat->name.'</span>';
        if($key < ($count-1))
        {
            echo ' ';
        }
        else
        {
            echo ' ';
        }
    }
}

add_action( 'widgets_init', 'wb_extra_widgets' );
/**
 * Register new Widget area for Product Cat sort dropdown.
 */
function wb_extra_widgets() {
	register_sidebar( array(
		'id'            => 'prod_sort',
		'name'          => __( 'Product Cat Sort', 'themename' ),
		'description'   => __( 'This site below Shop Title', 'themename' ),
		'before_widget'	=> '<div>',
		'after_widget'	=> '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>'
	) );
}

add_action( 'woocommerce_before_shop_loop','wb_prod_sort' ); // Hook it after headline and before loop
/**
 * Position the Product Category Sort dropdown.
 */
function wb_prod_sort() {
	if ( is_active_sidebar( 'prod_sort' ) ) {
		dynamic_sidebar( 'prod_sort' );
	}
}
/**
 * @snippet       Checkbox to display Custom Product Badge Part 1 - WooCommerce
 * @how-to        Get CustomizeWoo.com FREE
 * @source        https://businessbloomer.com/?p=17419
 * @author        Rodolfo Melogli
 * @compatible    Woo 3.5.3
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
// First, let's remove related products from their original position
 
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
 
// Second, let's add a new tab
 
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
 
function woo_new_product_tab( $tabs ) {
    
$tabs['related_products'] = array(
   'title'    => __( 'RELATED PRODUCTS', 'woocommerce' ),
   'priority'    => 50,
   'callback'    => 'woo_new_product_tab_content'
);
   return $tabs;
}
 
// Third, let's put the related products inside
 
function woo_new_product_tab_content() {
woocommerce_output_related_products();
}
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	$tabs['additional_information']['title'] = __( 'PRODUCT INFO' );	// Rename the additional information tab
	return $tabs;

}

add_filter( 'woocommerce_get_breadcrumb', function($crumbs, $Breadcrumb){
    $shop_page_id = wc_get_page_id('shop'); //Get the shop page ID
    if($shop_page_id > 0 && !is_shop()) { //Check we got an ID (shop page is set). Added check for is_shop to prevent Home / Shop / Shop as suggested in comments
        $new_breadcrumb = [
            _x( 'Shop', 'breadcrumb', 'woocommerce' ), //Title
            get_permalink(1297423) // URL
        ];
        array_splice($crumbs, 1, 0, [$new_breadcrumb]); //Insert a new breadcrumb after the 'Home' crumb
    }
    return $crumbs;
}, 10, 2 );

function remove_image_zoom_support() {
    remove_theme_support( 'wc-product-gallery-zoom' );
}
add_action( 'after_setup_theme', 'remove_image_zoom_support', 100 );